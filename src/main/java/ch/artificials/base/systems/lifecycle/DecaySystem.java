/**
 * Copyright by artificials 2017
 */
package ch.artificials.base.systems.lifecycle;

import ch.artificials.base.es.Decay;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntitySet;
import com.simsilica.sim.AbstractGameSystem;
import com.simsilica.sim.SimTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DecaySystem extends AbstractGameSystem {

    static Logger LOG = LoggerFactory.getLogger(DecaySystem.class.getName());

    private EntityData ed;
    private EntitySet entities;

    public DecaySystem() {
    }

    @Override
    public void initialize() {
        LOG.info("initialize");

        this.ed = getSystem(EntityData.class);
        this.entities = this.ed.getEntities(Decay.class);
    }

    @Override
    protected void terminate() {
        LOG.info("terminate");
        this.entities.release();
        this.entities = null;
    }

    @Override
    public void update(SimTime time) {
        LOG.debug("update time: {}", time);
        entities.applyChanges();
        entities.stream().forEach((e) -> {
            Decay decay = e.get(Decay.class);
            if (decay.getPercent() >= 1.0) {
                LOG.debug("decay: {}", decay);
                if (decay.getComponents().length == 0) {
                    LOG.debug("remove entity {}", e);
                    ed.removeEntity(e.getId());
                } else {
                    for (Class component : decay.getComponents()) {
                        LOG.debug("remove component {}", component);
                        ed.removeComponent(e.getId(), component);
                    }
                    ed.removeComponent(e.getId(), Decay.class);
                }
            }
        });

    }

}
