/*
 *  artificials.ch 2017
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j;

import com.simsilica.es.EntityId;

public interface Dyn4jBodyGetter {
    public Dyn4jBody getBody(EntityId entityId);
}
