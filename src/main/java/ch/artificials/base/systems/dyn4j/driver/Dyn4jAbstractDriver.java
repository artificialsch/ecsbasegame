/*
 *  artificials.ch 2018
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j.driver;

import ch.artificials.base.systems.dyn4j.Dyn4jBody;
import ch.artificials.base.systems.dyn4j.Dyn4jDriver;
import com.simsilica.es.EntityData;
import org.dyn4j.geometry.MassType;

abstract public class Dyn4jAbstractDriver implements Dyn4jDriver {

    protected final EntityData ed;
    protected Dyn4jBody body;

    public Dyn4jAbstractDriver(final EntityData ed) {
        this.ed = ed;
    }    
    
    @Override
    public void initialize(Dyn4jBody body) {
        this.body = body;
        body.setUserData(this);
        body.setAngularDamping(0);
        body.setLinearDamping(0);
        body.setMass(MassType.NORMAL);
    }
}
