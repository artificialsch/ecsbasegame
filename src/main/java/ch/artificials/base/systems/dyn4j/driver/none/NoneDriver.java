/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j.driver.none;

import ch.artificials.base.systems.dyn4j.Dyn4jBody;
import ch.artificials.base.systems.dyn4j.Dyn4jDriver;
import com.simsilica.es.EntityData;
import org.dyn4j.geometry.MassType;

public class NoneDriver implements Dyn4jDriver {

    public NoneDriver(EntityData ed) {
    }

    @Override
    public void update(final double stepTime) {
    }

    @Override
    public void initialize(Dyn4jBody body) {
        body.setMass(MassType.INFINITE);
    }

}
