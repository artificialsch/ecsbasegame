/*
 *  artificials.ch 2018
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j;

public interface Dyn4jDriver {

    public void update(final double stepTime);
    public void initialize(Dyn4jBody body);
}
