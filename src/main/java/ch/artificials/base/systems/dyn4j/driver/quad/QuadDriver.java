/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j.driver.quad;

import ch.artificials.base.Main;
import ch.artificials.base.systems.dyn4j.Dyn4jBody;
import ch.artificials.base.systems.dyn4j.Dyn4jDriver;
import ch.artificials.base.systems.dyn4j.driver.Dyn4jObjectDriver;
import com.simsilica.es.EntityData;
import javax.inject.Inject;
import org.dyn4j.geometry.Mass;
import org.dyn4j.geometry.Vector2;

public class QuadDriver extends Dyn4jObjectDriver implements Dyn4jDriver {

    @Inject
    public QuadDriver(Main main, EntityData ed) {
        super(ed);
    }

    @Override
    public void initialize(Dyn4jBody body) {
        super.initialize(body);
        body.getFixture(0).setRestitution(0.3);
        Mass mass = new Mass(new Vector2(0, 0), 4, 0);        
        body.setMass(mass);
    }
}
