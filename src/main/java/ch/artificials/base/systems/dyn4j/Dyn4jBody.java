/*
 *  artificials.ch 2018
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j;

import com.simsilica.es.EntityId;
import com.simsilica.mathd.Quatd;
import com.simsilica.mathd.Vec3d;
import org.dyn4j.dynamics.Body;
import org.dyn4j.geometry.Vector2;
import com.simsilica.mathd.AaBBox;


public class Dyn4jBody extends Body {

    private final EntityId entityId;
    private final AaBBox bounds;

    public Dyn4jBody(final EntityId entityId, AaBBox bounds) {
        this.entityId = entityId;
        this.bounds = bounds;
    }

    public EntityId getEntiyId() {
        return entityId;
    }

    public Quatd getOrientation() {
        return new Quatd()
                .fromAngles(0, 0, getTransform().getRotation());
    }

    public Vec3d getLocation() {
        Vector2 pos = getTransform().getTranslation();
        return new Vec3d(pos.x, pos.y, 0);
    }

    public AaBBox getBounds() {
        return bounds;
    }
}
