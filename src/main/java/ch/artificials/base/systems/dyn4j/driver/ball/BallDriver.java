/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j.driver.ball;

import ch.artificials.base.view.model.ball.*;
import ch.artificials.base.Main;
import ch.artificials.base.systems.dyn4j.Dyn4jBody;
import ch.artificials.base.systems.dyn4j.Dyn4jDriver;
import ch.artificials.base.systems.dyn4j.driver.Dyn4jObjectDriver;
import ch.artificials.base.view.model.Model;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.simsilica.es.EntityData;
import javax.inject.Inject;
import org.dyn4j.geometry.Mass;
import org.dyn4j.geometry.Vector2;

public class BallDriver extends Dyn4jObjectDriver implements Model, Dyn4jDriver {

    private final Node result;

    @Inject
    public BallDriver(Main main, EntityData ed) {
        super(ed);
        AssetManager assetManager = main.getAssetManager();
        result = (Node) assetManager.loadModel("Models/Ball.j3o");
    }

    @Override
    public Spatial getSpatial() {
        return result;
    }

    @Override
    public void initialize(Dyn4jBody body) {
        super.initialize(body);
        body.getFixture(0).setRestitution(0.3);
        body.getFixture(0).setFriction(0.1);
        Mass mass = new Mass(new Vector2(0, 0), 0.5, 0.5);        
        body.setMass(mass);
    }
}
