/*
 *  artificials.ch 2018
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j.driver;

import ch.artificials.base.systems.dyn4j.Dyn4jBody;
import ch.artificials.base.systems.dyn4j.Dyn4jDriver;
import com.simsilica.es.EntityData;
import org.dyn4j.geometry.MassType;

public class Dyn4jStaticDriver extends Dyn4jAbstractDriver {

    public static Dyn4jDriver create(final EntityData ed) {
        return new Dyn4jStaticDriver(ed);
    }

    public Dyn4jStaticDriver(final EntityData ed) {
        super(ed);
    }

    @Override
    public void update(final double stepTime) {
    }

    @Override
    public void initialize(Dyn4jBody body) {
        super.initialize(body);
        body.setMass(MassType.INFINITE);
    }
}
