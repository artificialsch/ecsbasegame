/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j;

import ch.artificials.base.EntityDataState;
import ch.artificials.base.Main;
import ch.artificials.base.es.Collidable;
import ch.artificials.base.es.DriverType;
import ch.artificials.base.es.Force;
import ch.artificials.base.es.Link;
import ch.artificials.base.es.Position;
import ch.artificials.base.es.PositionPropagation;
import ch.artificials.base.es.RectangleShape;
import ch.artificials.base.es.SphereShape;
import com.jme3.app.Application;
import com.jme3.app.state.BaseAppState;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityContainer;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import com.simsilica.mathd.AaBBox;
import com.simsilica.mathd.Quatd;
import com.simsilica.mathd.Vec3d;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.codejargon.feather.Feather;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.dynamics.RaycastResult;
import org.dyn4j.dynamics.Settings;

import org.dyn4j.dynamics.World;
import org.dyn4j.geometry.Convex;
import org.dyn4j.geometry.Geometry;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Ray;
import org.dyn4j.geometry.Transform;
import org.dyn4j.geometry.Vector2;

public class Dyn4jPhysicsSystem extends BaseAppState implements Dyn4jBodyGetter {

    private World world;
    private final Map<EntityId, Dyn4jBody> bodyCache;
    private final Map<EntityId, Map<EntityId, Vector3f>> forcesByParent;
    private EntityData ed;
    private SphereBodyContainer sphereBodyContainer;
    private RectangleBodyContainer rectangleBodyContainer;
    private ForceContainer forceContainer;
    private Feather feather;

    public Dyn4jPhysicsSystem() {
        world = new World();
        bodyCache = new ConcurrentHashMap<>();
        forcesByParent = new HashMap<>();
    }

    public static boolean isColliding(final Dyn4jBody b1, final Dyn4jBody b2) {
        if (b1 != null && b2 != null && !b1.equals(b2)) {
            return b1.isInContact(b2);
        }
        return false;
    }

    public boolean isClosestBody(final Vector2 origin, final Dyn4jBody target, Vector2 direction, final double radius) {
        if (origin != null && direction.getMagnitudeSquared() > 0.06) {
            Ray ray = new Ray(origin, direction);
            List<RaycastResult> results = new LinkedList<>();
            world.raycast(ray, radius, false, false, results);
            if (results.size() > 0) {
                return results.get(0).getBody() == target;
            }
        }
        return false;
    }

    @Override
    public Dyn4jBody getBody(final EntityId entityId) {
        return bodyCache.get(entityId);
    }

    @Override
    protected void initialize(Application app) {
        feather = ((Main) app).feather();

        world.setGravity(new Vector2(0.0, -30));
        Settings settings = world.getSettings();
        settings.setMaximumRotation(FastMath.DEG_TO_RAD * 18);
        settings.setMaximumTranslation(4);
        ed = app.getStateManager().getState(EntityDataState.class).getEntityData();
    }

    @Override
    protected void cleanup(Application app) {
        world.removeAllBodiesAndJoints();
        world.removeAllListeners();
        world = null;
    }

    @Override
    protected void onEnable() {
        sphereBodyContainer = new SphereBodyContainer(ed);
        sphereBodyContainer.start();
        rectangleBodyContainer = new RectangleBodyContainer(ed);
        rectangleBodyContainer.start();
        forceContainer = new ForceContainer(ed);
        forceContainer.start();
    }

    @Override
    protected void onDisable() {
        sphereBodyContainer.stop();
        sphereBodyContainer = null;
        rectangleBodyContainer.stop();
        rectangleBodyContainer = null;
        forceContainer.stop();
        forceContainer = null;
    }

    @Override
    public void update(final float tpf) {
        sphereBodyContainer.update();
        rectangleBodyContainer.update();
        forceContainer.update();

        updateDrivers(tpf);
        applyForces();
        world.update(tpf);
        propagateNewPositions();
    }

    private void updateDrivers(final float tpf) {
        for (Dyn4jBody body : sphereBodyContainer.getArray()) {
            Dyn4jDriver driver = (Dyn4jDriver) body.getUserData();
            driver.update(tpf);
        }
        for (Dyn4jBody body : rectangleBodyContainer.getArray()) {
            Dyn4jDriver driver = (Dyn4jDriver) body.getUserData();
            driver.update(tpf);
        }
    }

    private void applyForces() {
        forcesByParent.entrySet().stream()
                .filter(parent -> getBody(parent.getKey()) != null)
                .forEach(parent -> {
                    Dyn4jBody body = getBody(parent.getKey());
                    parent.getValue().values().stream().forEach(force -> {
                        body.applyImpulse(new Vector2(force.x, force.y));
                    });
                    parent.getValue().keySet().stream().forEach(entityId -> ed.removeEntity(entityId));
                });
        forcesByParent.clear();
    }

    private void propagateNewPositions() {
        for (Dyn4jBody body : sphereBodyContainer.getArray()) {
            PositionPropagation position = new PositionPropagation(body.getLocation(), body.getOrientation());
            ed.setComponent(body.getEntiyId(), position);
        }
        for (Dyn4jBody body : rectangleBodyContainer.getArray()) {
            PositionPropagation position = new PositionPropagation(body.getLocation(), body.getOrientation());
            ed.setComponent(body.getEntiyId(), position);
        }
    }

    protected Dyn4jBody createObject(final Entity entity, final AaBBox aabbox, final Convex shape) {
        Dyn4jBody body = new Dyn4jBody(entity.getId(), aabbox);
        BodyFixture bf = new BodyFixture(shape);
        body.addFixture(bf);
        Dyn4jDriver driver = (Dyn4jDriver) feather.instance(entity.get(DriverType.class).getType());
        driver.initialize(body);
        updateDyn4jBody(body, entity);
        setParentInitialSpeed(body, entity);
        bodyCache.put(entity.getId(), body);
        world.addBody(body);
        return body;
    }

    protected void updateDyn4jBody(final Dyn4jBody body, final Entity entity) {
        Quatd rot = entity.get(Position.class).getFacing();
        float angles[] = new float[3];
        rot.toQuaternion().toAngles(angles);
        body.rotate(-angles[2]);
        Vec3d pos = entity.get(Position.class).getLocation();
        Transform transform = new Transform();
        transform.setTranslation(pos.x, pos.y);
        body.setTransform(transform);
        BodyFixture bf = body.getFixture(0);
        if (ed.getComponent(entity.getId(), Collidable.class) != null) {
            bf.setSensor(false);
        } else {
            bf.setSensor(true);
        }
    }

    private void setParentInitialSpeed(final Dyn4jBody body, final Entity entity) {
        if (body.getMass().getType() == MassType.INFINITE) {
            return;
        }
        Link link = ed.getComponent(entity.getId(), Link.class);
        if (link != null) {
            Dyn4jBody parent = getBody(link.getParent());
            if (parent != null) {
                Vector2 linearVelocity = parent.getLinearVelocity();
                body.setLinearVelocity(linearVelocity.copy());
            }
        }
    }

    protected void removeDyn4jBody(final Dyn4jBody body, final Entity entity) {
        bodyCache.remove(entity.getId());
        world.removeBody(body);
    }

    private class SphereBodyContainer extends EntityContainer<Dyn4jBody> {

        public SphereBodyContainer(final EntityData ed) {
            super(ed, DriverType.class, Position.class, SphereShape.class);
        }

        @Override
        protected Dyn4jBody[] getArray() {
            return super.getArray();
        }

        @Override
        protected Dyn4jBody addObject(final Entity entity) {
            Dyn4jBody body = bodyCache.get(entity.getId());
            if (body == null) {
                double radius = entity.get(SphereShape.class).getRadius();
                body = createObject(entity, new AaBBox(radius), Geometry.createCircle(radius));
            }
            return body;
        }

        @Override
        protected void updateObject(final Dyn4jBody body, final Entity entity) {
            updateDyn4jBody(body, entity);
        }

        @Override
        protected void removeObject(final Dyn4jBody body, final Entity entity) {
            removeDyn4jBody(body, entity);
        }
    }

    private class RectangleBodyContainer extends EntityContainer<Dyn4jBody> {

        public RectangleBodyContainer(final EntityData ed) {
            super(ed, DriverType.class, Position.class, RectangleShape.class);
        }

        @Override
        protected Dyn4jBody[] getArray() {
            return super.getArray();
        }

        @Override
        protected Dyn4jBody addObject(final Entity entity) {
            Dyn4jBody body = bodyCache.get(entity.getId());
            if (body == null) {
                RectangleShape rectangle = entity.get(RectangleShape.class);
                double width = rectangle.getWidth();
                double height = rectangle.getHeight();
                body = createObject(entity, new AaBBox(Math.max(width, height)),
                        Geometry.createRectangle(width, height));
            }
            return body;
        }

        @Override
        protected void updateObject(final Dyn4jBody body, final Entity entity) {
            updateDyn4jBody(body, entity);
        }

        @Override
        protected void removeObject(final Dyn4jBody body, final Entity entity) {
            removeDyn4jBody(body, entity);
        }
    }

    class ForceData {

        EntityId parent;
        int index;
    }

    private class ForceContainer extends EntityContainer<ForceData> {

        public ForceContainer(EntityData ed) {
            super(ed, Force.class, Link.class);
        }

        @Override
        protected ForceData addObject(Entity entity) {
            ForceData data = new ForceData();
            updateObject(data, entity);
            return data;
        }

        @Override
        protected void updateObject(ForceData data, Entity entity) {
            data.parent = entity.get(Link.class).getParent();
            Map<EntityId, Vector3f> forces = forcesByParent.get(data.parent);
            if (forces == null) {
                forces = new HashMap<>();
                forcesByParent.put(data.parent, forces);
            }
            data.index = forces.size();
            forces.put(entity.getId(), entity.get(Force.class).getForce());
        }

        @Override
        protected void removeObject(ForceData data, Entity entity) {
            Map<EntityId, Vector3f> forces = forcesByParent.get(data.parent);
            if (forces != null) {
                forces.remove(entity.getId());
            }
        }

    }
}
