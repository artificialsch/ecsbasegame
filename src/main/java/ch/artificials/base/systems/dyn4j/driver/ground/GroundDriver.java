/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.dyn4j.driver.ground;

import ch.artificials.base.Main;
import ch.artificials.base.systems.dyn4j.Dyn4jBody;
import ch.artificials.base.systems.dyn4j.Dyn4jDriver;
import ch.artificials.base.systems.dyn4j.driver.Dyn4jStaticDriver;
import com.simsilica.es.EntityData;
import javax.inject.Inject;

public class GroundDriver extends Dyn4jStaticDriver implements Dyn4jDriver {

    @Inject
    public GroundDriver(Main main, EntityData ed) {
        super(ed);
    }
    
    @Override
    public void initialize(Dyn4jBody body) {
        super.initialize(body);
    }

}
