package ch.artificials.base.systems.mvc;

import com.jme3.input.KeyInput;
import com.simsilica.lemur.input.FunctionId;
import com.simsilica.lemur.input.InputMapper;
import com.simsilica.lemur.input.InputState;

public class PlayerMovementFunctions {

    public static final String G_MOVEMENT = "Movement";

    public static final FunctionId F_MOVE = new FunctionId(G_MOVEMENT, "Move");
    public static final FunctionId F_JUMP = new FunctionId(G_MOVEMENT, "Jump");

    public static void initializeDefaultMappings(InputMapper inputMapper) {

        if (!inputMapper.hasMappings(F_MOVE)) {
            inputMapper.map(F_MOVE, KeyInput.KEY_D);
            inputMapper.map(F_MOVE, InputState.Negative, KeyInput.KEY_A);
        }

        if (!inputMapper.hasMappings(F_JUMP)) {
            inputMapper.map(F_JUMP, KeyInput.KEY_W);
            inputMapper.map(F_JUMP, InputState.Negative, KeyInput.KEY_S);
        }
    }

}
