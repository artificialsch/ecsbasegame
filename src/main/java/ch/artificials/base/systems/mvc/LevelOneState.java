/*
 * artificials.ch 2016
 * all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.mvc;

import ch.artificials.base.EntityDataState;
import ch.artificials.base.Main;
import ch.artificials.base.es.Collidable;
import ch.artificials.base.es.DriverType;
import ch.artificials.base.es.ModelType;
import ch.artificials.base.es.Player;
import ch.artificials.base.es.Position;
import ch.artificials.base.es.RectangleShape;
import ch.artificials.base.systems.dyn4j.driver.ground.GroundDriver;
import ch.artificials.base.systems.dyn4j.driver.quad.QuadDriver;
import ch.artificials.base.view.model.ground.Ground40Model;
import ch.artificials.base.view.model.quad.QuadModel;
import com.jme3.app.Application;
import com.jme3.app.state.BaseAppState;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import com.simsilica.mathd.Vec3d;

public class LevelOneState extends BaseAppState {

    private EntityData ed;

    @Override
    protected void initialize(Application app) {
        Main main = (Main) app;
        ed = main.getStateManager().getState(EntityDataState.class).getEntityData();
        EntityId entityId = ed.createEntity();
        ed.setComponents(entityId,
                new ModelType(Ground40Model.class),
                new DriverType(GroundDriver.class),
                new Position(new Vec3d(0, 0, 0)),
                new RectangleShape(40, 0.5, new Vec3d()),
                new Collidable());

        entityId = ed.createEntity();
        ed.setComponents(entityId,
                new Player(0),
                new ModelType(QuadModel.class),
                new DriverType(QuadDriver.class),
                new Position(new Vec3d(0, 3, 0)),
                new RectangleShape(2, 2, new Vec3d()),
                new Collidable());
    }

    @Override
    protected void cleanup(Application app) {
    }

    @Override
    protected void onEnable() {
        // TODO: build level
    }

    @Override
    protected void onDisable() {
        // TODO: build level
    }
}
