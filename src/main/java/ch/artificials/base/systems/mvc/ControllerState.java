/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.mvc;

import ch.artificials.base.*;
import ch.artificials.base.es.Decay;
import ch.artificials.base.es.Force;
import ch.artificials.base.es.Link;
import ch.artificials.base.es.Player;
import com.jme3.app.Application;
import com.jme3.app.state.BaseAppState;
import com.jme3.input.ChaseCamera;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityContainer;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import com.simsilica.lemur.GuiGlobals;
import com.simsilica.lemur.input.AnalogFunctionListener;
import com.simsilica.lemur.input.FunctionId;
import com.simsilica.lemur.input.InputMapper;
import com.simsilica.lemur.input.InputState;
import com.simsilica.lemur.input.StateFunctionListener;

public class ControllerState extends BaseAppState implements AnalogFunctionListener, StateFunctionListener {
    
    private ChaseCamera chaseCamera;
    private Main main;
    private EntityData ed;
    private PlayerContainer playerContainer;
    private InputMapper inputMapper;
    private ModelViewSystem mvc;
    private Spatial playerOneModel;
    
    @Override
    protected void initialize(Application app) {
        main = (Main) app;
        
        ed = main.getStateManager().getState(EntityDataState.class).getEntityData();
        
        mvc = main.getStateManager().getState(ModelViewSystem.class);

        // TODO: Replace chase camera with my own one
        playerOneModel = null;
        chaseCamera = new ChaseCamera(app.getCamera(), new Node(), app.getInputManager());
        chaseCamera.setDefaultVerticalRotation(FastMath.DEG_TO_RAD * 10);
        chaseCamera.setDefaultHorizontalRotation(FastMath.DEG_TO_RAD * 90);
        if (inputMapper == null) {
            inputMapper = GuiGlobals.getInstance().getInputMapper();
        }

        // Most of the movement functions are treated as analog.        
        inputMapper.addAnalogListener(this, PlayerMovementFunctions.F_MOVE);
        inputMapper.addStateListener(this, PlayerMovementFunctions.F_JUMP);
    }
    
    @Override
    protected void cleanup(Application app) {
    }
    
    @Override
    protected void onEnable() {
        playerContainer = new PlayerContainer(ed);
        playerContainer.start();
        inputMapper.activateGroup(PlayerMovementFunctions.G_MOVEMENT);
        GuiGlobals.getInstance().setCursorEventsEnabled(false);
    }
    
    @Override
    protected void onDisable() {
        GuiGlobals.getInstance().setCursorEventsEnabled(true);
        inputMapper.deactivateGroup(PlayerMovementFunctions.G_MOVEMENT);
        playerContainer.stop();
        playerContainer = null;
    }
    
    @Override
    public void update(float tpf) {
        playerContainer.update();
        chaseCamera.update(tpf);
        if (playerOneModel == null && arePlayersReady()) {
            playerOneModel = mvc.getModel(getPlayer(0).entityId).getSpatial();
            chaseCamera.setSpatial(playerOneModel);
        }
    }
    
    @Override
    public void valueActive(FunctionId func, double value, double tpf) {
        if (arePlayersReady()) {
            Controller playerOne = getPlayer(0);
            // TODO: get number to take the right controler
            EntityId entityId = ed.createEntity();
            ed.setComponents(entityId,
                    new Decay(100, Force.class),
                    new Force(new Vector3f((float) value, 0, 0)),
                    new Link(playerOne.entityId));
        }
    }
    
    @Override
    public void valueChanged(FunctionId func, InputState value, double tpf) {
        if (arePlayersReady()) {
            Controller playerOne = getPlayer(0);
            if (func == PlayerMovementFunctions.F_JUMP) {
                if (value == InputState.Positive) {
                    EntityId entityId = ed.createEntity();
                    ed.setComponents(entityId,
                            new Decay(500, Force.class),
                            new Force(new Vector3f(0, 90, 0)),
                            new Link(playerOne.entityId));
                }
            }
        }
    }
    
    private boolean arePlayersReady() {
        return playerContainer.getArray().length > 0;
    }
    
    private Controller getPlayer(int i) {
        Controller playerOne = playerContainer.getArray()[i];
        return playerOne;
    }
    
    class Controller {
        
        EntityId entityId;
        int number;
    }
    
    private class PlayerContainer extends EntityContainer<Controller> {
        
        public PlayerContainer(EntityData ed) {
            super(ed, Player.class);
        }
        
        @Override
        protected Controller[] getArray() {
            return super.getArray();
        }
        
        @Override
        protected Controller addObject(Entity entity) {
            Player player = entity.get(Player.class);
            Controller contoller = new Controller();
            contoller.entityId = entity.getId();
            contoller.number = player.getNumber();
            // TODO: Attach player to controller, first find out how to have more than one controller
            return contoller;
        }
        
        @Override
        protected void updateObject(Controller controller, Entity entity) {
        }
        
        @Override
        protected void removeObject(Controller controller, Entity entity) {
            // TODO: Detach player from controller
        }
        
    }
}
