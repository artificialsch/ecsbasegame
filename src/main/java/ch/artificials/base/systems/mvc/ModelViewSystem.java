/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.systems.mvc;

import ch.artificials.base.view.model.Model;
import ch.artificials.base.EntityDataState;
import ch.artificials.base.Main;
import ch.artificials.base.es.ModelType;
import ch.artificials.base.es.Position;
import ch.artificials.base.es.PositionPropagation;
import org.slf4j.*;

import com.jme3.app.Application;
import com.jme3.app.state.BaseAppState;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.*;

import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.FXAAFilter;
import com.simsilica.es.Entity;
import com.simsilica.es.EntityContainer;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import java.util.HashMap;
import java.util.Map;

public class ModelViewSystem extends BaseAppState {

    static Logger LOG = LoggerFactory.getLogger(ModelViewSystem.class);

    private Node modelRoot;

    private FilterPostProcessor fpp;
    private Node guiRoot;
    private final Map<EntityId, Model> models;
    private Main main;
    private EntityData ed;
    private ModelContainer modelContainer;
    private MobModelContainer mobModelContainer;

    public ModelViewSystem() {
        this.models = new HashMap<>();
    }

    public Model getModel(EntityId entityId) {
        return models.get(entityId);
    }

    @Override
    protected void initialize(Application app) {
        modelRoot = new Node();
        guiRoot = new Node();
        main = (Main) app;

        this.fpp = new FilterPostProcessor(((Main) getApplication()).getAssetManager());

        FXAAFilter fxaa = new FXAAFilter();
        fpp.addFilter(fxaa);

        main.getViewPort().addProcessor(fpp);

        DirectionalLight light = new DirectionalLight();
        light.setDirection(new Vector3f(0, -1, -1));
        light.setColor(ColorRGBA.White.mult(1f));
        main.getRootNode().addLight(light);

        light = new DirectionalLight();
        light.setDirection(new Vector3f(0, 1, 1));
        light.setColor(ColorRGBA.White.mult(1f));
        main.getRootNode().addLight(light);

        ed = main.getStateManager().getState(EntityDataState.class).getEntityData();
    }

    @Override
    protected void cleanup(Application app) {
        ((Main) getApplication()).getViewPort().removeProcessor(fpp);
    }

    @Override
    protected void onEnable() {
        ((Main) getApplication()).getRootNode().attachChild(modelRoot);
        ((Main) getApplication()).getGuiNode().attachChild(guiRoot);
        modelContainer = new ModelContainer(ed);
        modelContainer.start();
        mobModelContainer = new MobModelContainer(ed);
        mobModelContainer.start();
    }

    @Override
    protected void onDisable() {
        mobModelContainer.stop();
        mobModelContainer = null;
        modelContainer.stop();
        modelContainer = null;
        modelRoot.removeFromParent();
        guiRoot.removeFromParent();
    }

    @Override
    public void update(float tpf) {
        modelContainer.update();
        mobModelContainer.update();
    }

    public Model addModel(Entity entity, Class entityType) {
        LOG.debug("addObject(entity={})", entity);
        Model model = models.get(entity.getId());
        if (model != null) {
            return model;
        }
        ModelType type = entity.get(ModelType.class);
        model = (Model) main.feather().instance(type.getType());
        updateModel(model, entity, entityType);
        models.put(entity.getId(), model);
        main.getRootNode().attachChild(model.getSpatial());
        return model;

    }

    protected void updateModel(Model model, Entity entity, Class entityType) {
        LOG.debug("updateModel(model={}, entity={})", model, entity);
        Position position = (Position) entity.get(entityType);
        model.getSpatial().setLocalTranslation(position.getLocation().toVector3f());
        model.getSpatial().setLocalRotation(position.getFacing().toQuaternion());
    }

    protected void removeModel(Model model, Entity entity) {
        LOG.debug("removeModel(model={}, entity={})", model, entity);
        models.remove(entity.getId());
        model.getSpatial().removeFromParent();
    }

    private class ModelContainer extends EntityContainer<Model> {

        public ModelContainer(EntityData ed) {
            super(ed, ModelType.class, Position.class);
        }

        @Override
        protected Model addObject(Entity entity) {
            return addModel(entity, Position.class);
        }

        @Override
        protected void updateObject(Model model, Entity entity) {
            updateModel(model, entity, Position.class);
        }

        @Override
        protected void removeObject(Model model, Entity entity) {
            removeModel(model, entity);
        }
    }

    private class MobModelContainer extends EntityContainer<Model> {

        public MobModelContainer(EntityData ed) {
            super(ed, ModelType.class, PositionPropagation.class);
        }

        @Override
        protected Model addObject(Entity entity) {
            return addModel(entity, PositionPropagation.class);
        }

        @Override
        protected void updateObject(Model model, Entity entity) {
            updateModel(model, entity, PositionPropagation.class);
        }

        @Override
        protected void removeObject(Model model, Entity entity) {
            removeModel(model, entity);
        }
    }
}
