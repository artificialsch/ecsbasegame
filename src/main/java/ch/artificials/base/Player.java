/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import javax.inject.Inject;

public class Player implements Telegraph {

    @Inject
    public Player() {
    }

    @Override
    public boolean handleMessage(Telegram telegram) {
        return false;
    }

}
