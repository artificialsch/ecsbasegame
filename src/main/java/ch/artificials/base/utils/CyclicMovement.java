/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.utils;

import com.jme3.math.FastMath;

public class CyclicMovement {

    private float cycle = 0;
    private float movement;
    private final float speed;
    private final float end;
    private float cycles;

    public CyclicMovement() {
        this(360, 0, 0);
    }

    public CyclicMovement(float speed, float start, float end) {
        this.speed = speed;
        this.end = end;
        this.cycle = start;
        this.cycles = start;
    }

    public void update(float tpf) {
        cycle += tpf * speed;
        if (cycle >= 360) {
            cycle -= 360;
        }
        if (end > 0) {
            cycles += tpf * speed;
        }
        if (end <= 0 || cycles < end) {
            movement = FastMath.sin(cycle * FastMath.DEG_TO_RAD);
        }
    }

    public float getMovement() {
        return movement;
    }
}
