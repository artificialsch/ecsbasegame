/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base;

import ch.artificials.base.systems.dyn4j.Dyn4jPhysicsSystem;
import ch.artificials.base.systems.gdxai.GdxAiSystem;
import ch.artificials.base.systems.mvc.ControllerState;
import ch.artificials.base.systems.mvc.ModelViewSystem;
import ch.artificials.base.systems.mvc.PlayerMovementFunctions;
import ch.artificials.base.systems.mvc.LevelOneState;
import com.jme3.app.SimpleApplication;
import com.jme3.material.TechniqueDef;
import com.simsilica.lemur.GuiGlobals;
import com.simsilica.lemur.style.BaseStyles;
import org.codejargon.feather.Feather;

public class Main extends SimpleApplication {

    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }
    private final Feather feather;

    public Main() {
        super(new PlayerState(),
                new EntityDataState(),
                new GdxAiSystem(), 
                new Dyn4jPhysicsSystem(),
                new LevelOneState(),
                new ModelViewSystem(),
                new ControllerState());
        feather = Feather.with(new MainModule(this));
    }
    
    @Override
    public void simpleInitApp() {        
                setPauseOnLostFocus(false);
        setDisplayFps(false);
        setDisplayStatView(false);

        GuiGlobals.initialize(this);

        GuiGlobals globals = GuiGlobals.getInstance();
        BaseStyles.loadGlassStyle();
        globals.getStyles().setDefaultStyle("glass");

        //MainGameFunctions.initializeDefaultMappings(globals.getInputMapper());
        PlayerMovementFunctions.initializeDefaultMappings(globals.getInputMapper());
        
        renderManager.setPreferredLightMode(TechniqueDef.LightMode.SinglePass);
        renderManager.setSinglePassLightBatchSize(1);
    }
    
    public Feather feather() {
        return feather;
    }
}
