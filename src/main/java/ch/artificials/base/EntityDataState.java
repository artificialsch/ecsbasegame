/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base;

import com.jme3.app.Application;
import com.jme3.app.state.BaseAppState;
import com.simsilica.es.EntityData;
import com.simsilica.es.base.DefaultEntityData;

public class EntityDataState extends BaseAppState {

    private DefaultEntityData ed;

    @Override
    protected void initialize(Application app) {
        ed = new DefaultEntityData();
    }
    
    @Override
    protected void cleanup(Application app) {
        ed.close();
        ed = null;
    }

    @Override
    protected void onEnable() {
    }

    @Override
    protected void onDisable() {
    }
    
    public EntityData getEntityData() {
        return ed;
    }
}
