/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import com.simsilica.mathd.Quatd;
import com.simsilica.mathd.Vec3d;

public class PositionPropagation extends Position {

    public PositionPropagation(Vec3d location, Quatd orientation) {
        super(location, orientation);
    }
}
