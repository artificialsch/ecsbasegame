/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import com.simsilica.es.EntityComponent;

public class Tag implements EntityComponent {
    
    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
