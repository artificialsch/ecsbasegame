/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import com.simsilica.es.EntityComponent;
import com.simsilica.mathd.Vec3d;

public class RectangleShape implements EntityComponent {

    private Vec3d offset;
    private double width;
    private double height;

    protected RectangleShape() {
    }

    public RectangleShape(double width, double height, Vec3d centerOffset) {
        this.width = width;
        this.height = height;
        this.offset = centerOffset;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public Vec3d getCenterOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[widht=" + width + ", height=" + height + ", centerOffset=" + offset + "]";
    }
}
