/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import ch.artificials.base.view.model.none.NoneModel;
import com.simsilica.es.EntityComponent;

public class ModelType implements EntityComponent {

    private Class type;

    public static ModelType create(Class typeName) {
        return new ModelType(typeName);
    }

    protected ModelType() {
        this(NoneModel.class);
    }

    public ModelType(Class type) {
        this.type = type;
    }

    public Class getType() {
        return type;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[type=" + type + "]";
    }
}
