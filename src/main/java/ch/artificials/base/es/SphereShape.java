/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import com.simsilica.es.EntityComponent;
import com.simsilica.mathd.Vec3d;

public class SphereShape implements EntityComponent {

    private double radius;
    private Vec3d offset;

    protected SphereShape() {
    }

    public SphereShape(double radius, Vec3d centerOffset) {
        this.radius = radius;
        this.offset = centerOffset;
    }

    public double getRadius() {
        return radius;
    }

    public Vec3d getCenterOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "SphereShape[radius=" + radius + ", centerOffset=" + offset + "]";
    }
}
