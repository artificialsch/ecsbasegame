/*
 * artificials.ch 2016
 * all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import com.simsilica.es.EntityComponent;

/**
 * Represents a time-to-live for an entity.
 *
 * @author Christian
 */
public class Decay implements EntityComponent {

    private final long start;
    public long delta;
    private Class[] components;

    public Decay() {
        this.start = System.nanoTime();
    }

    public Decay(long deltaMillis, Class... components) {
        this.start = System.nanoTime();
        this.delta = deltaMillis * 1000000;
        this.components = components;
    }

    public double getPercent() {
        long time = System.nanoTime();
        return (double) (time - start) / delta;
    }

    public Class[] getComponents() {
        return components;
    }

    @Override
    public String toString() {
        String ret = "Decay[" + (delta / 1000000.0) + " ms";
        if (components.length > 0) {
            for (Class component : components) {
                ret += ", ";
                ret += component.getSimpleName();
            }
        }
        ret += "]";
        return ret;
    }
}
