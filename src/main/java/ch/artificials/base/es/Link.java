/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import com.simsilica.es.EntityComponent;
import com.simsilica.es.EntityId;

public class Link implements EntityComponent {

    private EntityId parentId;

    public Link() {
        parentId = new EntityId();
    }
    
    public Link(EntityId parentId) {
        this.parentId = parentId;
    }
    
    public EntityId getParent() {
        return parentId;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[" + parentId + "]";
    }
}
