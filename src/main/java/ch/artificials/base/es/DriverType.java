/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import ch.artificials.base.view.model.none.NoneModel;
import com.simsilica.es.EntityComponent;

public class DriverType implements EntityComponent {

    private Class type;

    public static DriverType create(Class typeName) {
        return new DriverType(typeName);
    }

    protected DriverType() {
        this(NoneModel.class);
    }

    public DriverType(Class type) {
        this.type = type;
    }

    public Class getType() {
        return type;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[type=" + type + "]";
    }
}
