/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import com.jme3.math.Vector3f;
import com.simsilica.es.EntityComponent;

public class Force implements EntityComponent {

    private final Vector3f force;

    public Force(Vector3f force) {
        this.force = force;
    }

    public Vector3f getForce() {
        return force;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[" + force + "]";
    }
}
