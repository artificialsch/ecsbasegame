/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import com.simsilica.es.EntityComponent;
import com.simsilica.mathd.Quatd;
import com.simsilica.mathd.Vec3d;

public class Position implements EntityComponent {

    private Vec3d location;
    private Quatd facing;

    public Position() {
        this(0, 0, 0);
    }

    public Position(double x, double y, double z) {
        this(new Vec3d(x, y, z), new Quatd());
    }

    public Position(Vec3d loc) {
        this(loc, new Quatd());
    }

    public Position(Vec3d loc, Quatd quat) {
        this.location = loc;
        this.facing = quat;
    }

    public Position changeLocation(Vec3d location) {
        return new Position(location, facing);
    }

    public Position changeFacing(Quatd facing) {
        return new Position(location, facing);
    }

    public Vec3d getLocation() {
        return location;
    }

    public Quatd getFacing() {
        return facing;
    }

    @Override
    public String toString() {
        return "Position[location=" + location + ", facing=" + facing + "]";
    }
}
