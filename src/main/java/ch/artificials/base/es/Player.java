/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.es;

import com.simsilica.es.EntityComponent;

public class Player implements EntityComponent {

    private final int number;

    public Player() {
        number = 0;
    }
    
    public Player(int number) {
        this.number = number;
    }
    
    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[number=" + number + "]";
    }
}
