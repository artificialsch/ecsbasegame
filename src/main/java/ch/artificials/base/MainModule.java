/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base;

import com.simsilica.es.EntityData;
import javax.inject.Singleton;
import org.codejargon.feather.Provides;

public class MainModule {

    private final Main main;

    public MainModule(final Main main) {
        this.main = main;
    }
    
    @Provides
    @Singleton
    public Main main() {
        return main;
    }
    
    @Provides
    @Singleton
    public EntityData ed() {
        return main.getStateManager().getState(EntityDataState.class).getEntityData();
    }
}
