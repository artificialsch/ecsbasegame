/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base;

import com.jme3.app.Application;
import com.jme3.app.state.BaseAppState;

public class PlayerState extends BaseAppState {

    private Player player;

    @Override
    protected void initialize(Application app) {
        player = ((Main) app).feather().instance(Player.class);
    }
    
    @Override
    protected void cleanup(Application app) {
    }

    @Override
    protected void onEnable() {
    }

    @Override
    protected void onDisable() {
    }
    
    public Player getPlayer() {
        return player;
    }
}
