/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.view.model.none;

import ch.artificials.base.view.model.Model;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class NoneModel implements Model {

    private final Node none;

    public NoneModel() {
        none = new Node();
    }
    
    @Override
    public Spatial getSpatial() {
        return none;
    }
}
