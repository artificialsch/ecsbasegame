/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.view.model;

import com.jme3.scene.Spatial;

public interface Model {
    public Spatial getSpatial();
}
