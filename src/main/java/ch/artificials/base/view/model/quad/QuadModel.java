/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.view.model.quad;

import ch.artificials.base.Main;
import ch.artificials.base.view.model.Model;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.simsilica.es.EntityData;
import javax.inject.Inject;

public class QuadModel implements Model {

    private final Node result;

    @Inject
    public QuadModel(Main main, EntityData ed) {
        AssetManager assetManager = main.getAssetManager();
        result = (Node) assetManager.loadModel("Models/Quad.j3o");
    }

    @Override
    public Spatial getSpatial() {
        return result;
    }
}
