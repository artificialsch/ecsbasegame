/*
 *  artificials.ch 2019
 *  all rights reserved by christian liesch<liesch@gmx.ch>
 */
package ch.artificials.base.exceptions;

public class FluffyModelNotFound extends Exception {

    public FluffyModelNotFound(String message, Throwable ex) {
        super(message, ex);
    }
}
